import { OutOfBoxProjectPage } from './app.po';

describe('out-of-box-project App', () => {
  let page: OutOfBoxProjectPage;

  beforeEach(() => {
    page = new OutOfBoxProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
